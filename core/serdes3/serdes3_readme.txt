The following files were generated for 'serdes3' in directory
C:\xlx_wd\DAQLSC_K7_ISE_V1\DaqLSC_sources_svn\

Generate XCO file:
   CORE Generator input file containing the parameters used to generate a core.

   * serdes3.xco

Associated Files Generator:
   Please see the core data sheet.

   * serdes3/doc/gtwizard_v2_3_vinfo.html
   * serdes3/doc/ug769_gtwizard.pdf
   * serdes3/example_design/chipscope_icon.ngc
   * serdes3/example_design/chipscope_ila.ngc
   * serdes3/example_design/chipscope_vio.ngc
   * serdes3/example_design/data_vio.ngc
   * serdes3/example_design/icon.ngc
   * serdes3/example_design/ila.ngc
   * serdes3/gtwizard_v2_3_readme.txt
   * serdes3/implement/v7ht.tcl
   * serdes3/src/gtwizard_v2_3_beachfront.v

Generate EJava outputs:
   Please see the core data sheet.

   * serdes3/cdc/filelist
   * serdes3/cdc/makefile
   * serdes3/example_design/gt_attributes.ucf
   * serdes3/example_design/gt_rom_init_rx.dat
   * serdes3/example_design/gt_rom_init_tx.dat
   * serdes3/example_design/serdes3_adapt_starter.vhd
   * serdes3/example_design/serdes3_adapt_top_dfe.vhd
   * serdes3/example_design/serdes3_adapt_top_lpm.vhd
   * serdes3/example_design/serdes3_agc_loop_fsm.vhd
   * serdes3/example_design/serdes3_ctle_agc_comp.vhd
   * serdes3/example_design/serdes3_exdes.ucf
   * serdes3/example_design/serdes3_exdes.vhd
   * serdes3/example_design/serdes3_exdes.xcf
   * serdes3/example_design/serdes3_exdes.xdc
   * serdes3/example_design/serdes3_exdes_synplify.sdc
   * serdes3/example_design/serdes3_gt_frame_check.vhd
   * serdes3/example_design/serdes3_gt_frame_gen.vhd
   * serdes3/example_design/serdes3_gt_usrclk_source.vhd
   * serdes3/example_design/serdes3_init.vhd
   * serdes3/example_design/serdes3_lpm_loop_fsm.vhd
   * serdes3/example_design/serdes3_recclk_monitor.vhd
   * serdes3/example_design/serdes3_rx_startup_fsm.vhd
   * serdes3/example_design/serdes3_tx_startup_fsm.vhd
   * serdes3/implement/chipscope_project.cpj
   * serdes3/implement/implement.bat
   * serdes3/implement/implement.sh
   * serdes3/implement/implement_synplify.bat
   * serdes3/implement/implement_synplify.sh
   * serdes3/implement/synplify.prj
   * serdes3/implement/vivado_rdn.bat
   * serdes3/implement/vivado_rdn.sh
   * serdes3/implement/vivado_rdn.tcl
   * serdes3/implement/xst.prj
   * serdes3/implement/xst.scr
   * serdes3/serdes3.pf
   * serdes3/simulation/functional/gt_rom_init_rx.dat
   * serdes3/simulation/functional/gt_rom_init_tx.dat
   * serdes3/simulation/functional/simulate_isim.bat
   * serdes3/simulation/functional/simulate_isim.sh
   * serdes3/simulation/functional/simulate_mti.bat
   * serdes3/simulation/functional/simulate_mti.do
   * serdes3/simulation/functional/simulate_mti.sh
   * serdes3/simulation/functional/simulate_ncsim.bat
   * serdes3/simulation/functional/simulate_ncsim.sh
   * serdes3/simulation/functional/simulate_vcs.sh
   * serdes3/simulation/functional/simulate_xsim.bat
   * serdes3/simulation/functional/simulate_xsim.sh
   * serdes3/simulation/functional/ucli_commands.key
   * serdes3/simulation/functional/vcs_session.tcl
   * serdes3/simulation/functional/wave_isim.tcl
   * serdes3/simulation/functional/wave_mti.do
   * serdes3/simulation/functional/wave_ncsim.sv
   * serdes3/simulation/functional/wave_xsim.tcl
   * serdes3/simulation/serdes3_tb.vhd
   * serdes3/simulation/serdes3_tb_imp.vhd
   * serdes3/simulation/sim_reset_gt_model.vhd
   * serdes3/simulation/timing/simulate_mti.bat
   * serdes3/simulation/timing/simulate_mti.do
   * serdes3/simulation/timing/simulate_mti.sh
   * serdes3.vhd
   * serdes3.vho
   * serdes3.xdc
   * serdes3_gt.vhd
   * serdes3_xmdf.tcl

Generate ISE project file:
   ISE Project Navigator support files. These are generated files and should not
   be edited directly.

   * serdes3.gise
   * serdes3.xise

Deliver Readme:
   Readme file for the IP.

   * serdes3_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * serdes3_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

