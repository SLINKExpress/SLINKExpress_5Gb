###############################################################################
##
## (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
##
## This file contains confidential and proprietary information
## of Xilinx, Inc. and is protected under U.S. and
## international copyright and other intellectual property
## laws.
##
## DISCLAIMER
## This disclaimer is not a license and does not grant any
## rights to the materials distributed herewith. Except as
## otherwise provided in a valid license issued to you by
## Xilinx, and to the maximum extent permitted by applicable
## law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
## WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
## AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
## BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
## INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
## (2) Xilinx shall not be liable (whether in contract or tort,
## including negligence, or under any other theory of
## liability) for any loss or damage of any kind or nature
## related to, arising under or in connection with these
## materials, including for any direct, or any indirect,
## special, incidental, or consequential loss or damage
## (including loss of data, profits, goodwill, or any type of
## loss or damage suffered as a result of any action brought
## by a third party) even if such damage or loss was
## reasonably foreseeable or Xilinx had been advised of the
## possibility of the same.
##
## CRITICAL APPLICATIONS
## Xilinx products are not designed or intended to be fail-
## safe, or for use in any application requiring fail-safe
## performance, such as life-support or safety devices or
## systems, Class III medical devices, nuclear facilities,
## applications related to the deployment of airbags, or any
## other applications that could lead to death, personal
## injury, or severe property or environmental damage
## (individually and collectively, "Critical
## Applications"). Customer assumes the sole risk and
## liability of any use of Xilinx products in Critical
## Applications, subject only to applicable laws and
## regulations governing limitations on product liability.
## 
## THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
## PART OF THIS FILE AT ALL TIMES.

  window new WaveWindow  -name  "Waves for 7 Series FPGAs Transceivers Wizard Example Design"
  waveform  using  "Waves for 7 Series FPGAs Transceivers Wizard Example Design"
   waveform  add  -label FRAME_CHECK_MODULE -comment gt0_frame_check
  waveform  add  -signals  :serdes3_exdes_i:gt0_frame_check:begin_r
  waveform  add  -signals  :serdes3_exdes_i:gt0_frame_check:track_data_r
  waveform  add  -siganls  :serdes3_exdes_i:gt0_frame_check:data_error_detected_r
  wavefrom  add  -siganls  :serdes3_exdes_i:gt0_frame_check:start_of_packet_detected_r
  waveform  add  -signals  :serdes3_exdes_i:gt0_frame_check:RX_DATA
  waveform  add  -signals  :serdes3_exdes_i:gt0_frame_check:ERROR_COUNT
  waveform  add  -label GT0_serdes3 -comment GT0_serdes3
  waveform  add  -label Channel  -comment  Channel
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.QPLLCLK_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.QPLLREFCLK_IN
  waveform  add  -label Channel_-_Dynamic_Reconfiguration_Port_(DRP)  -comment  Channel_-_Dynamic_Reconfiguration_Port_(DRP)
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPADDR_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPCLK_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPDI_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPDO_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPEN_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPRDY_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.DRPWE_IN
  waveform  add  -label Eye_Scan_Ports  -comment  Eye_Scan_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.EYESCANDATAERROR_OUT
  waveform  add  -label Loopback_and_Powerdown_Ports  -comment  Loopback_and_Powerdown_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.LOOPBACK_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXPD_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXPD_IN
  waveform  add  -label Receive_Ports  -comment  Receive_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXUSERRDY_IN
  waveform  add  -label Receive_Ports_-_8b10b_Decoder  -comment  Receive_Ports_-_8b10b_Decoder
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXCHARISCOMMA_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXCHARISK_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXDISPERR_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXNOTINTABLE_OUT
  waveform  add  -label Receive_Ports_-_Clock_Correction_Ports  -comment  Receive_Ports_-_Clock_Correction_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXCLKCORCNT_OUT
  waveform  add  -label Receive_Ports_-_Comma_Detection_and_Alignment  -comment  Receive_Ports_-_Comma_Detection_and_Alignment
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXBYTEISALIGNED_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXBYTEREALIGN_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXCOMMADET_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXMCOMMAALIGNEN_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXPCOMMAALIGNEN_IN
  waveform  add  -label Receive_Ports_-_RX_Data_Path_interface  -comment  Receive_Ports_-_RX_Data_Path_interface
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.GTRXRESET_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXDATA_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXOUTCLK_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXPMARESET_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXUSRCLK_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXUSRCLK2_IN
  waveform  add  -label Receive_Ports_-_RX_Decision_Feedback_Equalizer(DFE)  -comment  Receive_Ports_-_RX_Decision_Feedback_Equalizer(DFE)
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXDFEAGCHOLD_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXDFELPMRESET_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXMONITOROUT_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXMONITORSEL_IN
  waveform  add  -label Receive_Ports_-_RX_Driver,OOB_signalling,Coupling_and_Eq.,CDR  -comment  Receive_Ports_-_RX_Driver,OOB_signalling,Coupling_and_Eq.,CDR
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.GTXRXN_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.GTXRXP_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXCDRLOCK_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXELECIDLE_OUT
  waveform  add  -label Receive_Ports_-_RX_Elastic_Buffer_and_Phase_Alignment_Ports  -comment  Receive_Ports_-_RX_Elastic_Buffer_and_Phase_Alignment_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXBUFSTATUS_OUT
  waveform  add  -label Receive_Ports_-_RX_PLL_Ports  -comment  Receive_Ports_-_RX_PLL_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXRESETDONE_OUT
  waveform  add  -label Receive_Ports_-_RX_Polarity_Control_Ports  -comment  Receive_Ports_-_RX_Polarity_Control_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.RXPOLARITY_IN
  waveform  add  -label Transmit_Ports  -comment  Transmit_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXUSERRDY_IN
  waveform  add  -label Transmit_Ports_-_8b10b_Encoder_Control_Ports  -comment  Transmit_Ports_-_8b10b_Encoder_Control_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXCHARISK_IN
  waveform  add  -label Transmit_Ports_-_TX_Data_Path_interface  -comment  Transmit_Ports_-_TX_Data_Path_interface
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.GTTXRESET_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXDATA_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXOUTCLK_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXOUTCLKFABRIC_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXOUTCLKPCS_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXUSRCLK_IN
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXUSRCLK2_IN
  waveform  add  -label Transmit_Ports_-_TX_Driver_and_OOB_signaling  -comment  Transmit_Ports_-_TX_Driver_and_OOB_signaling
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.GTXTXN_OUT
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.GTXTXP_OUT
  waveform  add  -label Transmit_Ports_-_TX_PLL_Ports  -comment  Transmit_Ports_-_TX_PLL_Ports
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXRESETDONE_OUT
  waveform  add  -label Transmit_Ports_-_TX_Polarity_Control  -comment  Transmit_Ports_-_TX_Polarity_Control
  waveform  add  -signals  serdes3_TB.serdes3_exdes_i.serdes3_init_i.serdes3_i.gt0_serdes3_i.TXPOLARITY_IN


  console submit -using simulator -wait no "run 300 us"

