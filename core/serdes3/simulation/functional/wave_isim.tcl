###############################################################################
##
## (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
##
## This file contains confidential and proprietary information
## of Xilinx, Inc. and is protected under U.S. and
## international copyright and other intellectual property
## laws.
##
## DISCLAIMER
## This disclaimer is not a license and does not grant any
## rights to the materials distributed herewith. Except as
## otherwise provided in a valid license issued to you by
## Xilinx, and to the maximum extent permitted by applicable
## law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
## WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
## AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
## BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
## INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
## (2) Xilinx shall not be liable (whether in contract or tort,
## including negligence, or under any other theory of
## liability) for any loss or damage of any kind or nature
## related to, arising under or in connection with these
## materials, including for any direct, or any indirect,
## special, incidental, or consequential loss or damage
## (including loss of data, profits, goodwill, or any type of
## loss or damage suffered as a result of any action brought
## by a third party) even if such damage or loss was
## reasonably foreseeable or Xilinx had been advised of the
## possibility of the same.
##
## CRITICAL APPLICATIONS
## Xilinx products are not designed or intended to be fail-
## safe, or for use in any application requiring fail-safe
## performance, such as life-support or safety devices or
## systems, Class III medical devices, nuclear facilities,
## applications related to the deployment of airbags, or any
## other applications that could lead to death, personal
## injury, or severe property or environmental damage
## (individually and collectively, "Critical
## Applications"). Customer assumes the sole risk and
## liability of any use of Xilinx products in Critical
## Applications, subject only to applicable laws and
## regulations governing limitations on product liability.
## 
## THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
## PART OF THIS FILE AT ALL TIMES.



wcfg new
wave add /serdes3_TB/serdes3_exdes_i/gt0_frame_check/begin_r
wave add /serdes3_TB/serdes3_exdes_i/gt0_frame_check/track_data_r
wave add /serdes3_TB/serdes3_exdes_i/gt0_frame_check/data_error_detected_r
wave add /serdes3_TB/serdes3_exdes_i/gt0_frame_check/start_of_packet_detected_r
wave add /serdes3_TB/serdes3_exdes_i/gt0_frame_check/RX_DATA_IN
wave add /serdes3_TB/serdes3_exdes_i/gt0_frame_check/ERROR_COUNT_OUT
divider add "Channel"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/QPLLCLK_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/QPLLREFCLK_IN
divider add "Channel - Dynamic Reconfiguration Port (DRP)"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPADDR_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPCLK_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPDI_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPDO_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPEN_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPRDY_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/DRPWE_IN
divider add "Eye Scan Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/EYESCANDATAERROR_OUT
divider add "Loopback and Powerdown Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/LOOPBACK_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXPD_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXPD_IN
divider add "Receive Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXUSERRDY_IN
divider add "Receive Ports - 8b10b Decoder"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXCHARISCOMMA_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXCHARISK_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXDISPERR_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXNOTINTABLE_OUT
divider add "Receive Ports - Clock Correction Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXCLKCORCNT_OUT
divider add "Receive Ports - Comma Detection and Alignment"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXBYTEISALIGNED_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXBYTEREALIGN_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXCOMMADET_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXMCOMMAALIGNEN_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXPCOMMAALIGNEN_IN
divider add "Receive Ports - RX Data Path interface"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/GTRXRESET_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXDATA_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXOUTCLK_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXPMARESET_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXUSRCLK_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXUSRCLK2_IN
divider add "Receive Ports - RX Decision Feedback Equalizer(DFE)"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXDFEAGCHOLD_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXDFELPMRESET_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXMONITOROUT_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXMONITORSEL_IN
divider add "Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/GTXRXN_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/GTXRXP_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXCDRLOCK_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXELECIDLE_OUT
divider add "Receive Ports - RX Elastic Buffer and Phase Alignment Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXBUFSTATUS_OUT
divider add "Receive Ports - RX PLL Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXRESETDONE_OUT
divider add "Receive Ports - RX Polarity Control Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/RXPOLARITY_IN
divider add "Transmit Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXUSERRDY_IN
divider add "Transmit Ports - 8b10b Encoder Control Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXCHARISK_IN
divider add "Transmit Ports - TX Data Path interface"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/GTTXRESET_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXDATA_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXOUTCLK_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXOUTCLKFABRIC_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXOUTCLKPCS_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXUSRCLK_IN
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXUSRCLK2_IN
divider add "Transmit Ports - TX Driver and OOB signaling"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/GTXTXN_OUT
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/GTXTXP_OUT
divider add "Transmit Ports - TX PLL Ports"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXRESETDONE_OUT
divider add "Transmit Ports - TX Polarity Control"
wave add /serdes3_TB/serdes3_exdes_i/serdes3_init_i/serdes3_i/gt0_serdes3_i/TXPOLARITY_IN

run 300 us
quit



