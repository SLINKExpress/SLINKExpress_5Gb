
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;


library UNISIM;
use UNISIM.VComponents.all;

--****************************************************************************
--*
--*   Daq Link Source Card - FPGA core     
--*
--*   version 5E000003 Released on 17 October 2014
--*
--****************************************************************************







entity DaqLSC is
	 Generic (gtx_rxpolarity : std_logic := '0';
				 gtx_txpolarity : std_logic := '0');
				 
    Port ( sys_reset 	: in  STD_LOGIC;									-- active high reset of all logic but GTX
           sys_clk		: in  STD_LOGIC;									-- Input clock for logic circuitry, not used by SERDES
			  --
           LinkWe			: in  STD_LOGIC;									-- active low Link Write Enable
           LinkCtrl 		: in  STD_LOGIC;									-- active low, the data is transmitted as a Kontrol word
           LinkData 		: in  STD_LOGIC_VECTOR (63 downto 0); 		-- 64 bit wide link data
           srcID 			: in  STD_LOGIC_VECTOR (15 downto 0);		-- Unique identifier of the data source
           LinkDown 		: out  STD_LOGIC;									-- when low, link is not enabled to transfer data
           LinkFull 		: out  STD_LOGIC;									-- when low, indicates that the link can still receive 16 data words max.
																					-- Additionnal data after those 16 will be lost.
			  --
			  sync_loss 	: out STD_LOGIC;		  							-- goes to '1' (rxusrclk) when SERDES is out of synch
			  status_ce 	: in std_logic;									-- not implemented yet
			  status_addr 	: in STD_LOGIC_VECTOR (15 downto 0);		-- Address of internal status registers (see table below)
			  status_port 	: out STD_LOGIC_VECTOR (63 downto 0);		-- Content of addressed status registers
			  --
			  txusrclk_o 	: out STD_LOGIC;  								-- reconstructed tx clock, to be used to clock sending circuitry
			  rxusrclk_o 	: out STD_LOGIC;  								-- reconstructed rx clock, to be used to clock receiving circuitry
			  --
			  --			  --  Register table.
			  -- 
			  --
			  --  ADDRESS(HEX)           NAME                    Description
			  --
			  --	   0002					Data_cnt				Number of 64 bit words transfered
           --     0003					Event_cnt			Number of events transfered
			  --     0004					Block_cnt			Number of data blocks transfered
			  --     0005					Rec_Packet_cnt		Number of received packets
			  --		0006					Status_core_1		Status of internal state machine, only for experts
			  --		0007					Sent_Packet_cnt	Number of sent packets
			  --		0008					Status_core_2		Status of internal state machine, only for experts
			  --		0009					Back_P_cnt			Number of clock cycles with LinkFull active
			  --		000a					Version				Design version number
			  --		000b					Serdes				Status of internal SERDES, only for experts
			  --     000c					Retrans_Blck_cnt	Number of data blocks retransmitted			  
			  --  
			  gtx_reset 	: in std_logic;    								-- active high reset of GTX only
			  gtx_refclk_n : in std_logic; 									-- iob for GTX refclk neg
			  gtx_refclk_p : in std_logic; 									-- iob for GTX refclk pos
			  sfp_rxn 		: in std_logic;      							-- sfp iobs rx neg
			  sfp_rxp 		: in std_logic;									-- sfp iobs rx pos
			  sfp_txn 		: out std_logic;									-- sfp iobs tx neg
			  sfp_txp 		: out std_logic									-- sfp iobs tx pos
			 );
end DaqLSC;



architecture Behavioral of DaqLSC is

-- Local signals

signal sys_reset_bar : std_logic;
signal txusrclk, txusrclk2, rxusrclk, rxusrclk2 : std_logic;
signal txdll_locked, rxdll_locked, dll_reset, rxdll_reset, txdll_reset, txplllkdet, rxplllkdet: std_logic;
signal txclkfromserdes, rxclkfromserdes, rxclkfromserdes_bufg : std_logic;
signal serdes_in_sync : std_logic;
signal txdata, rxdata : std_logic_vector(31 downto 0);
signal rxcharisk, txcharisk, rxchariscomma : std_logic_vector(3 downto 0);
signal rxlossofsync : std_logic_vector(1 downto 0);
signal rxbufstatus : std_logic_vector(2 downto 0);
signal rxbyteisaligned, rxenrealign, rxbyterealign, rxcommadet : std_logic;
signal gtxrefclk, drp_clk   : std_logic;
signal gtx_cpllfbclklost, gtx_cplllock, gtx_cpllrefclklost, gtx_rxresetdone, gtx_txresetdone : std_logic;
signal gtx_qplllock, gtx_qpllrefclklost, gtx_rxcdrlock, gtx_rxbyteisaligned_is_stable : std_logic;
signal stable_count : std_logic_vector(15 downto 0) ;
signal serdes_status :std_logic_vector(31 downto 0);




	COMPONENT serdes3_GT_USRCLK_SOURCE
	PORT(
		Q2_CLK0_GTREFCLK_PAD_N_IN : IN std_logic;
		Q2_CLK0_GTREFCLK_PAD_P_IN : IN std_logic;
		GT0_TXOUTCLK_IN : IN std_logic;
		DRPCLK_IN : IN std_logic;          
		Q2_CLK0_GTREFCLK_OUT : OUT std_logic;
		GT0_TXUSRCLK_OUT : OUT std_logic;
		GT0_TXUSRCLK2_OUT : OUT std_logic;
		GT0_RXUSRCLK_OUT : OUT std_logic;
		GT0_RXUSRCLK2_OUT : OUT std_logic;
		DRPCLK_OUT : OUT std_logic
		);
	END COMPONENT;

	COMPONENT SLINK_opt
	PORT(
		reset : IN std_logic;
		SYS_CLK : IN std_logic;
		LINKWe : IN std_logic;
		LINKCtrl : IN std_logic;
		LINKData : IN std_logic_vector(63 downto 0);
		src_ID : IN std_logic_vector(15 downto 0);
		inject_err : IN std_logic_vector(17 downto 0);
		read_CE : IN std_logic;
		Addr : IN std_logic_vector(15 downto 0);
		clock : IN std_logic;
		serdes_init : IN std_logic;
		clock_r : IN std_logic;
		SD_Data_i : IN std_logic_vector(31 downto 0);
		SD_Kb_i : IN std_logic_vector(3 downto 0);
		Serdes_status : IN std_logic_vector(31 downto 0);          
		status_data : OUT std_logic_vector(63 downto 0);
		LINKDown : OUT std_logic;
		LINK_LFF : OUT std_logic;
		SD_Data_o : OUT std_logic_vector(31 downto 0);
		SD_Kb_o : OUT std_logic_vector(3 downto 0)

		);
	END COMPONENT;


component serdes3_init 
generic
(
    -- Simulation attributes
    EXAMPLE_SIM_GTRESET_SPEEDUP    : string    := "FALSE";    -- Set to 1 to speed up sim reset
    EXAMPLE_SIMULATION             : integer   := 0;          -- Set to 1 for simulation
    EXAMPLE_USE_CHIPSCOPE          : integer   := 0           -- Set to 1 to use Chipscope to drive resets

);
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_IN                           : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X0Y10)
    --____________________________CHANNEL PORTS________________________________
    ---------------- Channel - Dynamic Reconfiguration Port (DRP) --------------
    GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT0_DRPCLK_IN                           : in   std_logic;
    GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT0_DRPEN_IN                            : in   std_logic;
    GT0_DRPRDY_OUT                          : out  std_logic;
    GT0_DRPWE_IN                            : in   std_logic;
    ------------------------------- Eye Scan Ports -----------------------------
    GT0_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------ Loopback and Powerdown Ports ----------------------
    GT0_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    GT0_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT0_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    ------------------------------- Receive Ports ------------------------------
    GT0_RXUSERRDY_IN                        : in   std_logic;
    ----------------------- Receive Ports - 8b10b Decoder ----------------------
    GT0_RXCHARISCOMMA_OUT                   : out  std_logic_vector(3 downto 0);
    GT0_RXCHARISK_OUT                       : out  std_logic_vector(3 downto 0);
    GT0_RXDISPERR_OUT                       : out  std_logic_vector(3 downto 0);
    GT0_RXNOTINTABLE_OUT                    : out  std_logic_vector(3 downto 0);
    ------------------- Receive Ports - Clock Correction Ports -----------------
    GT0_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
    --------------- Receive Ports - Comma Detection and Alignment --------------
    GT0_RXBYTEISALIGNED_OUT                 : out  std_logic;
    GT0_RXBYTEREALIGN_OUT                   : out  std_logic;
    GT0_RXCOMMADET_OUT                      : out  std_logic;
    GT0_RXMCOMMAALIGNEN_IN                  : in   std_logic;
    GT0_RXPCOMMAALIGNEN_IN                  : in   std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
    GT0_GTRXRESET_IN                        : in   std_logic;
    GT0_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    GT0_RXUSRCLK_IN                         : in   std_logic;
    GT0_RXUSRCLK2_IN                        : in   std_logic;
    ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
    GT0_RXDFELPMRESET_IN                    : in   std_logic;
    GT0_RXMONITOROUT_OUT                    : out  std_logic_vector(6 downto 0);
    GT0_RXMONITORSEL_IN                     : in   std_logic_vector(1 downto 0);
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    GT0_GTXRXN_IN                           : in   std_logic;
    GT0_GTXRXP_IN                           : in   std_logic;
    GT0_RXCDRLOCK_OUT                       : out  std_logic;
    GT0_RXELECIDLE_OUT                      : out  std_logic;
    -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    GT0_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    ------------------------ Receive Ports - RX PLL Ports ----------------------
    GT0_RXRESETDONE_OUT                     : out  std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    GT0_RXPOLARITY_IN                       : in   std_logic;
    ------------------------------- Transmit Ports -----------------------------
    GT0_TXUSERRDY_IN                        : in   std_logic;
    ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
    GT0_TXCHARISK_IN                        : in   std_logic_vector(3 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT0_GTTXRESET_IN                        : in   std_logic;
    GT0_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    GT0_TXOUTCLK_OUT                        : out  std_logic;
    GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
    GT0_TXUSRCLK_IN                         : in   std_logic;
    GT0_TXUSRCLK2_IN                        : in   std_logic;
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT0_GTXTXN_OUT                          : out  std_logic;
    GT0_GTXTXP_OUT                          : out  std_logic;
    ----------------------- Transmit Ports - TX PLL Ports ----------------------
    GT0_TXRESETDONE_OUT                     : out  std_logic;
    -------------------- Transmit Ports - TX Polarity Control ------------------
    GT0_TXPOLARITY_IN                       : in   std_logic;
   

    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    GT0_GTREFCLK0_COMMON_IN                 : in   std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    GT0_QPLLLOCK_OUT                        : out  std_logic;
    GT0_QPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_QPLLRESET_IN                        : in   std_logic


);
end component;



begin





Inst_serdes3_GT_USRCLK_SOURCE: serdes3_GT_USRCLK_SOURCE
PORT MAP(

		Q2_CLK0_GTREFCLK_PAD_N_IN => gtx_refclk_n,
		Q2_CLK0_GTREFCLK_PAD_P_IN => gtx_refclk_p,
		Q2_CLK0_GTREFCLK_OUT => gtxrefclk,
		
		GT0_TXUSRCLK_OUT => txusrclk,
		GT0_TXUSRCLK2_OUT => txusrclk2,
		GT0_TXOUTCLK_IN => TXCLKfromSERDES,
		GT0_RXUSRCLK_OUT => open,
		GT0_RXUSRCLK2_OUT => open,
		DRPCLK_IN => sys_clk,
		DRPCLK_OUT => drp_clk 
);
	



Inst_SLINK_opt: SLINK_opt
PORT MAP(

-- FROM FED logic	
		reset => sys_reset_bar, -- needs an active low reset
		SYS_CLK => sys_clk,
		
		
-- DATA interface from FED

		
		LINKWe => LinkWe,
		LINKCtrl => LinkCtrl,
		LINKData => LinkData,
		src_ID => srcID,
		inject_err => (others =>'0'),
		read_CE => '0',
		Addr => status_addr,
		status_data => status_port,
		serdes_status => serdes_status,
		LINKDown => LinkDown,
		LINK_LFF => LinkFull,
		
-- SERDES interface		
		
		clock => txusrclk2,         		-- clk tx from SERDES
		serdes_init => serdes_in_sync, 	-- status that comes back from GTX
		SD_Data_o => TXDATA, 			-- data sent to serdes (32 bit)
		SD_Kb_o => TXCHARISK,         -- control K associated to SD_Data_o (4 bits)
		clock_r => txusrclk2,         -- reconstructed clock from SERDES
		SD_Data_i => RXDATA,          -- return data from SERDES 32 bit
		SD_Kb_i => RXCHARISK          -- return control K associated to SD_Data_i (4 bits)

);
	
serdes3_init_i : serdes3_init
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     =>      "FALSE",
        EXAMPLE_SIMULATION              =>      0,
        EXAMPLE_USE_CHIPSCOPE           =>      0
    )
    port map
    (
        SYSCLK_IN                       =>      sys_clk,
        SOFT_RESET_IN                   =>      sys_reset,
        GT0_TX_FSM_RESET_DONE_OUT       =>      open,
        GT0_RX_FSM_RESET_DONE_OUT       =>      open,
        GT0_DATA_VALID_IN               =>      '0',

        --GT0  (X0Y10)


        ---------------- Channel - Dynamic Reconfiguration Port (DRP) --------------
        GT0_DRPADDR_IN                  =>      "000000000",
        GT0_DRPCLK_IN                   =>      drp_clk,
        GT0_DRPDI_IN                    =>      x"0000",
        GT0_DRPDO_OUT                   =>      open,
        GT0_DRPEN_IN                    =>      '0',
        GT0_DRPRDY_OUT                  =>      open,
        GT0_DRPWE_IN                    =>      '0',
        ------------------------------- Eye Scan Ports -----------------------------
        GT0_EYESCANDATAERROR_OUT        =>      open,
        ------------------------ Loopback and Powerdown Ports ----------------------
        GT0_LOOPBACK_IN                 =>      "000",
        GT0_RXPD_IN                     =>      "00",
        GT0_TXPD_IN                     =>      "00",
        ------------------------------- Receive Ports ------------------------------
        GT0_RXUSERRDY_IN                =>      '1',
        ----------------------- Receive Ports - 8b10b Decoder ----------------------
        GT0_RXCHARISCOMMA_OUT           =>      rxchariscomma,
        GT0_RXCHARISK_OUT               =>      rxcharisk,
        GT0_RXDISPERR_OUT               =>      open,
        GT0_RXNOTINTABLE_OUT            =>      open,
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT0_RXCLKCORCNT_OUT             =>      open,
        --------------- Receive Ports - Comma Detection and Alignment --------------
        GT0_RXBYTEISALIGNED_OUT         =>      rxbyteisaligned,
        GT0_RXBYTEREALIGN_OUT           =>      rxbyterealign,
        GT0_RXCOMMADET_OUT              =>      rxcommadet,
        GT0_RXMCOMMAALIGNEN_IN          =>      '0', --rxenrealign,
        GT0_RXPCOMMAALIGNEN_IN          =>      rxenrealign,
        ------------------- Receive Ports - RX Data Path interface -----------------
        GT0_GTRXRESET_IN                =>      gtx_reset,
        GT0_RXDATA_OUT                  =>      rxdata,
        GT0_RXUSRCLK_IN                 =>      txusrclk,
        GT0_RXUSRCLK2_IN                =>      txusrclk,
        ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
        GT0_RXDFELPMRESET_IN            =>      '0',
        GT0_RXMONITOROUT_OUT            =>      open,
        GT0_RXMONITORSEL_IN             =>      "00",
        ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        GT0_GTXRXN_IN                   =>      sfp_rxn,
        GT0_GTXRXP_IN                   =>      sfp_rxp,
        GT0_RXCDRLOCK_OUT               =>      gtx_rxcdrlock,
        GT0_RXELECIDLE_OUT              =>      open,
        -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        GT0_RXBUFSTATUS_OUT             =>      rxbufstatus,
        ------------------------ Receive Ports - RX PLL Ports ----------------------
        GT0_RXRESETDONE_OUT             =>      gtx_rxresetdone,
        ----------------- Receive Ports - RX Polarity Control Ports ----------------
        GT0_RXPOLARITY_IN               =>      gtx_rxpolarity,
        ------------------------------- Transmit Ports -----------------------------
        GT0_TXUSERRDY_IN                =>      '1',
        ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
        GT0_TXCHARISK_IN                =>      txcharisk, --x"f"
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_GTTXRESET_IN                =>      gtx_reset,
        GT0_TXDATA_IN                   =>      txdata, --x"7c_bc_dc_1c"
        GT0_TXOUTCLK_OUT                =>      TXCLKfromSERDES,
        GT0_TXOUTCLKFABRIC_OUT          =>      open,
        GT0_TXOUTCLKPCS_OUT             =>      open,
        GT0_TXUSRCLK_IN                 =>      txusrclk,
        GT0_TXUSRCLK2_IN                =>      txusrclk,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  =>      sfp_txn,
        GT0_GTXTXP_OUT                  =>      sfp_txp,
        ----------------------- Transmit Ports - TX PLL Ports ----------------------
        GT0_TXRESETDONE_OUT             =>      gtx_txresetdone,
        -------------------- Transmit Ports - TX Polarity Control ------------------
        GT0_TXPOLARITY_IN               =>      gtx_txpolarity,


    
    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
        GT0_GTREFCLK0_COMMON_IN         =>      gtxrefclk,
    ------------------------- Common Block - QPLL Ports ------------------------
        GT0_QPLLLOCK_OUT                =>      gtx_qplllock,
        GT0_QPLLLOCKDETCLK_IN           =>      drp_clk,
--        GT0_QPLLREFCLKLOST_OUT          =>      gtx_qpllrefclklost,
        GT0_QPLLRESET_IN                =>      '0'

     
);
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

txusrclk_o <= txusrclk;
rxusrclk_o <= txusrclk;
sys_reset_bar <= not(sys_reset);



process (txusrclk) 
begin
   if txusrclk='1' and txusrclk'event then
      if rxbyteisaligned ='0' then 
         stable_count						<= (others => '0');
			gtx_rxbyteisaligned_is_stable <= '0';
      else 
         stable_count <= stable_count + 1;
				if stable_count = x"ffff" then 
					gtx_rxbyteisaligned_is_stable <= '1'; 
				end if;
      end if;
		
		serdes_in_sync <= gtx_rxresetdone and gtx_txresetdone and gtx_rxbyteisaligned_is_stable; --removed to avoid inti frames to be resent if cdrlock goes down.
		sync_loss 		<= not(serdes_in_sync);
		rxenrealign 	<= not(rxbyteisaligned);		
		
   end if;
end process; 




serdes_status(0) <= gtx_qplllock;
serdes_status(1) <= gtx_qpllrefclklost;
serdes_status(2) <= gtx_txresetdone;
serdes_status(3) <= gtx_rxresetdone;

serdes_status(4) <= gtx_rxcdrlock;
serdes_status(5) <= rxbyteisaligned;
serdes_status(6) <= rxbyterealign;
serdes_status(7) <= rxcommadet;
 
serdes_status(11 downto 8) <= rxchariscomma;
serdes_status(15 downto 12) <= rxcharisk;

serdes_status(16) <= gtx_cpllfbclklost;
serdes_status(17) <= gtx_cplllock;
serdes_status(18) <= gtx_cpllrefclklost;








end Behavioral;

